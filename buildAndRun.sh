#!/bin/bash
mvn clean install
if [ $? -eq 0 ]; then
   java -jar target/exchange-rate-checker-*.jar --debug
else
   printf '\n\nmaven clean install FAIL!\n\n'
fi
