# Exchange Rate Checker Euro Dollar

Candidate Task "Senior Java Developer"

Develop a service, that constantly checks the currency exchange rate from Euro to US-Dollar (1 EUR = x USD).

Requirements:

· The check period has to be configurable

· The results are stored in a database. The database access does not need to be fully implemented, an interface is sufficient.

· The service has an HTTP-Resource with the following endpoints (Theprotocol design is up to you):

o Get latest rate

o Get historical rates from startDate to endDate

· Please ensure the functionality of the business logic using unit-tests

· The exchange rate can be taken from a public service or be mocked.

· The project should be managed with maven and the tests have to be executable using 'mvn test'.

· Please describe in a few sentences and/or with a diagram how you planned the project and architecture.


# Summary of Implementation

A micro-service was developed using Spring Boot, Feign Client, JPA, H2 Memory Database and Spring Scheduled to create a periodically running Job.

A job keeps running periodically to look for the exchange rate of the euro against the dollar. 

Exchange rates are stored in the in-memory database.

In the configuration file *application.yml* you can configure Job execution using the CRON format and configure the time zone.

For example, run every 10 minutes, Monday through Friday from 7 AM to 9 PM in the Central European Time:

> cron: 0 0/10 7-21 * * MON-FRI

> timezone: CET



Spring Boot Actuator has been enabled and you can verify that the service is running properly through the endpoint '/health'

Example of call: `curl -X GET localhost:8080/health`
> Response: { "status": "UP" }


There are two endpoints with business features:

#### /api/v1/latest

Example of call: `curl -X GET localhost:8080/api/v1/latest`
> Response: { "id":5, "rate":1.16," datetime":1537328858316 }

#### /api/v1/historical/{startDate}/{endDate}

Example of call: `curl -X GET localhost:8080/api/v1/historical/2018-09-17-10-30/2018-09-17-11-45`
> Response: [ { "id": 1, "rate": 1.2, "datetime": 1537329338316 }, { "id": 2, "rate": 1.19, "datetime": 1537329278316 }, { "id": 3, "rate": 1.18, "datetime": 1537329218316 }, { "id": 4, "rate": 1.17, "datetime": 1537329098316 }, { "id": 5, "rate": 1.16, "datetime": 1537328858316 } ]


The shell script *buildAndRun.sh* serves to facilitate the process of build + execution of the micro service.

About the tests: Unit tests and contract tests(calling endpoints) were implemented. Using Junit, Mockito and MockMvc.


**Have fun!**