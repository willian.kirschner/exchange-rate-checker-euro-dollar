package de.com.searchmetrics.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import de.com.searchmetrics.business.service.ExchangeRateService;
import de.com.searchmetrics.business.service.InvalidDataTimeFormatException;
import de.com.searchmetrics.entity.ExchangeRate;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
@CrossOrigin // I put this annotation so you guys have no CORS issues!
@RestController
@RequestMapping("/api/v1")
public class ApiController {

	@Autowired
	private ExchangeRateService exchangeRateService;

	@GetMapping("/latest")
	public ResponseEntity<?> getLatest() {
		ExchangeRate latest = exchangeRateService.getLatestRate();
		if (latest == null) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(latest);
	}

	@GetMapping("/historical/{startDate}/{endDate}")
	public ResponseEntity<?> getHistoricalRates(@PathVariable String startDate, @PathVariable String endDate) {
		List<ExchangeRate> historical;
		try {
			historical = exchangeRateService.getHistoricalRates(startDate, endDate);
		} catch (InvalidDataTimeFormatException e) {
			return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		if (historical == null || historical.isEmpty()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(historical);
	}

}
