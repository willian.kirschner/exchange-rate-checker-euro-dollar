package de.com.searchmetrics.entity;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
public interface ExchangeRateRepository extends CrudRepository<ExchangeRate, Long> {

	ExchangeRate findFirstByOrderByIdDesc();

	List<ExchangeRate> findByDatetimeBetween(Timestamp DatetimeStart, Timestamp DatetimeEnd);

}
