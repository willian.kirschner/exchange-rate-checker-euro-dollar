package de.com.searchmetrics.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
@Entity
public class ExchangeRate {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private BigDecimal rate;

	@Column(nullable = false)
	private Timestamp datetime;

	public ExchangeRate() {
	}

	public ExchangeRate(BigDecimal rate, Timestamp datetime) {
		this.rate = rate;
		this.datetime = datetime;
	}

	public Long getId() {
		return id;
	}

	public BigDecimal getRate() {
		return rate;
	}

	public Timestamp getDatetime() {
		return datetime;
	}

	@Override
	public String toString() {
		return "ExchangeRate [id=" + id + ", rate=" + rate + ", datetime=" + datetime + "]";
	}

}
