package de.com.searchmetrics;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

import de.com.searchmetrics.entity.ExchangeRate;
import de.com.searchmetrics.entity.ExchangeRateRepository;

/**
 * 
 * @author Willian Kirschner willkev@gmail.com
 */
@SpringBootApplication
@EnableFeignClients
@ComponentScan("de.com.searchmetrics")
public class Application {

	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	// Just to start, inserts some fake records
	public Application(ExchangeRateRepository repository) {
		long now = new Date().getTime();
		repository.save(new ExchangeRate(new BigDecimal(1.20), new Timestamp(now)));
		repository.save(new ExchangeRate(new BigDecimal(1.19), new Timestamp(now - 60000)));
		repository.save(new ExchangeRate(new BigDecimal(1.18), new Timestamp(now - 120000)));
		repository.save(new ExchangeRate(new BigDecimal(1.17), new Timestamp(now - 240000)));
		repository.save(new ExchangeRate(new BigDecimal(1.16), new Timestamp(now - 480000)));

		for (ExchangeRate exchangeRate : repository.findAll()) {
			LOGGER.info("result mocked findAll: {}", exchangeRate);
		}
	}

}
