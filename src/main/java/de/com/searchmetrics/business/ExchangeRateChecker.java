package de.com.searchmetrics.business;

import java.math.BigDecimal;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import de.com.searchmetrics.business.service.ExchangeRateService;
import de.com.searchmetrics.entity.ExchangeRate;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
@Service
public class ExchangeRateChecker {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExchangeRateChecker.class);

	@Value("${data-fixer-io-api.access_key}")
	private String accessKey;

	@Value("${data-fixer-io-api.symbols}")
	private String symbols;

	@Autowired
	private RatesFeignClient ratesFeignClient;

	@Autowired
	private ExchangeRateService exchangeRateService;

	@Scheduled(cron = "${job.checker.cron}", zone = "${job.checker.timezone}")
	public void job() {
		LOGGER.info("Started Job...");
		if (checkLatestRate()) {
			LOGGER.info("End Job with Sucess!");
		} else {
			LOGGER.error("End Job with no Sucess.");
		}
	}

	public boolean checkLatestRate() {
		RatesEUR latest = ratesFeignClient.getLatest(accessKey, symbols);

		BigDecimal rate;
		Timestamp exchangeRateTime;
		try {
			rate = new BigDecimal(latest.getRates().get("USD"));
		} catch (Exception e) {
			LOGGER.error("Could not convert exchange rate value.", e);
			return false;
		}
		try {
			exchangeRateTime = new Timestamp(Long.parseLong(latest.getTimestamp()));
		} catch (Exception e) {
			LOGGER.error("Could not convert exchange rate time.", e);
			return false;
		}
		ExchangeRate exRate = new ExchangeRate(rate, exchangeRateTime);
		exchangeRateService.updateExchangeRate(exRate);
		return true;
	}

}
