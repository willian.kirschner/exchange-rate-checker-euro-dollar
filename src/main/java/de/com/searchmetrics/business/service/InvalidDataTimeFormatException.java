package de.com.searchmetrics.business.service;

public class InvalidDataTimeFormatException extends Exception {

	public InvalidDataTimeFormatException() {
		super();
	}

	public InvalidDataTimeFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public InvalidDataTimeFormatException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidDataTimeFormatException(String message) {
		super(message);
	}

	public InvalidDataTimeFormatException(Throwable cause) {
		super(cause);
	}

}
