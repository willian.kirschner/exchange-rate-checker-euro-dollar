package de.com.searchmetrics.business.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.com.searchmetrics.entity.ExchangeRate;
import de.com.searchmetrics.entity.ExchangeRateRepository;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
@Service
public class ExchangeRateService {

	private static final SimpleDateFormat FORMATER = new SimpleDateFormat("yyyy-MM-dd-HH-mm");

	private final ExchangeRateRepository exchangeRateRepository;

	@Autowired
	public ExchangeRateService(ExchangeRateRepository exchangeRateRepository) {
		this.exchangeRateRepository = exchangeRateRepository;
	}

	public ExchangeRate getLatestRate() {
		return exchangeRateRepository.findFirstByOrderByIdDesc();
	}

	public List<ExchangeRate> getHistoricalRates(String startDate, String endDate)
			throws InvalidDataTimeFormatException {
		try {
			Timestamp startTimestamp = new Timestamp(FORMATER.parse(startDate == null ? "" : startDate).getTime());
			Timestamp endTimestamp = new Timestamp(FORMATER.parse(endDate == null ? "" : endDate).getTime());

			return exchangeRateRepository.findByDatetimeBetween(startTimestamp, endTimestamp);
		} catch (ParseException e) {
			throw new InvalidDataTimeFormatException("Expected dateTime format is yyyy-MM-dd-HH-mm", e);
		}
	}

	public void updateExchangeRate(ExchangeRate exchangeRate) {
		exchangeRateRepository.save(exchangeRate);
	}

}
