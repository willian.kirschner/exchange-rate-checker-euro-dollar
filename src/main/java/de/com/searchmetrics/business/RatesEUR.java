package de.com.searchmetrics.business;

import java.util.HashMap;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
public class RatesEUR {

	private String success;
	private String timestamp;
	private String base;
	private String date;
	private HashMap<String, Double> rates;

	public RatesEUR() {
	}

	public RatesEUR(String success, String timestamp, String base, String date, HashMap<String, Double> rates) {
		super();
		this.success = success;
		this.timestamp = timestamp;
		this.base = base;
		this.date = date;
		this.rates = rates;
	}

	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getBase() {
		return base;
	}

	public void setBase(String base) {
		this.base = base;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public HashMap<String, Double> getRates() {
		return rates;
	}

	public void setRates(HashMap<String, Double> rates) {
		this.rates = rates;
	}

	@Override
	public String toString() {
		return "Rates [success=" + success + ", timestamp=" + timestamp + ", base=" + base + ", date=" + date
				+ ", rates=" + rates + "]";
	}

}
