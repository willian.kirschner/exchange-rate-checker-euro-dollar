package de.com.searchmetrics.business;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
@FeignClient("exchange-rates-public-api")
public interface RatesFeignClient {

	@RequestMapping(method = RequestMethod.GET, value = "/latest")
	RatesEUR getLatest(@RequestParam("access_key") String accessKey, @RequestParam("symbols") String symbols);

}
