package de.com.searchmetrics.business.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import de.com.searchmetrics.entity.ExchangeRate;
import de.com.searchmetrics.entity.ExchangeRateRepository;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
@RunWith(MockitoJUnitRunner.class)
public class ExchangeRateServiceTest {

	@Mock
	private ExchangeRateRepository exchangeRateRepository;

	@Test(expected = InvalidDataTimeFormatException.class)
	public void test_fail_date_format1() throws InvalidDataTimeFormatException {
		ExchangeRateService exchangeRateService = new ExchangeRateService(exchangeRateRepository);
		exchangeRateService.getHistoricalRates("startDate", "endDate");
	}

	@Test(expected = InvalidDataTimeFormatException.class)
	public void test_fail_date_format2() throws InvalidDataTimeFormatException {
		ExchangeRateService exchangeRateService = new ExchangeRateService(exchangeRateRepository);
		exchangeRateService.getHistoricalRates(null, null);
	}

	@Test(expected = InvalidDataTimeFormatException.class)
	public void test_fail_date_format3() throws InvalidDataTimeFormatException {
		ExchangeRateService exchangeRateService = new ExchangeRateService(exchangeRateRepository);
		exchangeRateService.getHistoricalRates("2018-09-17", "2018-09-17-12");
	}

	@Test
	public void test_date_format1() throws InvalidDataTimeFormatException {
		List<ExchangeRate> historicalRates = new ArrayList<>();
		when(exchangeRateRepository.findByDatetimeBetween(any(), any())).thenReturn(historicalRates);

		ExchangeRateService exchangeRateService = new ExchangeRateService(exchangeRateRepository);
		historicalRates = exchangeRateService.getHistoricalRates("2018-09-17-12-34", "2018-09-17-12-34");
		Assert.assertNotNull(historicalRates);
	}

}
