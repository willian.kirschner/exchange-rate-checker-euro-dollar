package de.com.searchmetrics.business.service;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isOneOf;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import de.com.searchmetrics.api.v1.ApiController;
import de.com.searchmetrics.entity.ExchangeRate;

/**
 *
 * @author Willian Kirschner willkev@gmail.com
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
public class ExchangeRateServiceMockMvcTest {

	private static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
	
	@Mock
	private ExchangeRateService exchangeRateService;

	@InjectMocks
	private ApiController apiController;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(apiController).alwaysDo(print()).build();
	}

	@Test
	public void test_get_latest_ok() throws Exception {
		BigDecimal rateValue = new BigDecimal(123.45);
		Timestamp datetime = new Timestamp(new Date().getTime());
		ExchangeRate latest = new ExchangeRate(rateValue, datetime);
		when(exchangeRateService.getLatestRate()).thenReturn(latest);

		mockMvc.perform(get("/api/v1/latest"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$.rate", equalTo(rateValue)))
			.andExpect(jsonPath("$.datetime", any(Long.class)));
	}

	@Test
	public void test_get_latest_empty() throws Exception {
		when(exchangeRateService.getLatestRate()).thenReturn(null);

		mockMvc.perform(get("/api/v1/latest"))
			.andDo(print())
			.andExpect(status().isNoContent());
	}

	@Test
	@Ignore
	public void test_get_historical_ok() throws Exception {
		BigDecimal rateValue1 = new BigDecimal(123.45);
		BigDecimal rateValue2 = new BigDecimal(111.11);
		BigDecimal rateValue3 = new BigDecimal(9.01);
		Timestamp datetime = new Timestamp(new Date().getTime());
		
		ExchangeRate rate1 = new ExchangeRate(rateValue1, datetime);
		ExchangeRate rate2 = new ExchangeRate(rateValue2, datetime);
		ExchangeRate rate3 = new ExchangeRate(rateValue3, datetime);
		
		when(exchangeRateService.getHistoricalRates(org.mockito.Matchers.any(), org.mockito.Matchers.any()))
				.thenReturn(Arrays.asList(rate1, rate2, rate3));

		String startDate = "2018-09-17-23-01";
		String endDate = "2018-09-17-23-15";
		mockMvc.perform(get("/api/v1/historical/{startDate}/{endDate}", startDate, endDate))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(APPLICATION_JSON_UTF8))
			.andExpect(jsonPath("$", hasSize(3)))
			.andExpect(jsonPath("$[0].rate", isOneOf(rate1, rate2, rate3)))
			.andExpect(jsonPath("$[0].datetime", any(Long.class)))
			.andExpect(jsonPath("$[2].rate", isOneOf(rate1, rate2, rate3)))
			.andExpect(jsonPath("$[2].datetime", any(Long.class)));
	}
	
	
	@Test
	public void test_get_historical_empty() throws Exception {
		when(exchangeRateService.getHistoricalRates(org.mockito.Matchers.any(), org.mockito.Matchers.any()))
				.thenReturn(null);

		mockMvc.perform(get("/api/v1/latest"))
			.andDo(print())
			.andExpect(status().isNoContent());
	}	
	
}
